<?php

include('include/autoloader.php');
session_start();

$uri = rtrim( dirname($_SERVER["SCRIPT_NAME"]), '/' );
$uri = '/' . trim( str_replace( $uri, '', $_SERVER['REQUEST_URI'] ), '/' );
$uri = urldecode( $uri );

$baseUrl = str_replace('index.php','',$_SERVER["SCRIPT_NAME"]);
//$baseUrl = $_SERVER["SCRIPT_NAME"];

$routes = array(
    'home'            => "/",                           // '/'
    'user/picture'    => "/picture/(?'id'\d+)",         // '/picture/51'
    'admin/home'      => "/admin",                      // '/admin'
    'admin/users'     => "/admin/users",                // '/admin/users'
    'admin/user'      => "/page/user/(?'id'\d+)",       // '/admin/user/7'
    'admin/edit_user' => "/page/user/(?'id'\d+)/edit",  // '/admin/user/7/edit'
);

foreach ($routes as $action => $route)
{
    if ( preg_match( '~^'.$route.'$~i', $uri, $routeParameters ) )
    {
        cleanup($routeParameters);
        include( 'Controller/'.$action.'.php' );
        exit();
    }
}
include( 'View/404.html.php' );


function cleanup($array) {
    foreach ($array as $key=>$value){
        if(gettype($key)=='integer'){
            unset($array[$key]);
        }
    }
}